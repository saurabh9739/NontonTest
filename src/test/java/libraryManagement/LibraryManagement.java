package test.java.libraryManagement;

import org.testng.annotations.Test;

import main.java.pageObject.LoginPage;
import main.java.pageObject.Videos;
import main.java.testBase.TestBase;

public class LibraryManagement extends TestBase {
	
	LoginPage login;
	Videos videos;
	
	@Test(priority =1)
	public void login()
	{
		login = new LoginPage(driver);
		login.enterEmail(OR.getProperty("username"));
		login.enterPassword(OR.getProperty("password"));
		login.clickSignInWithValidCredentials();
		log.info("login test completed");
	}

	@Test(priority=2)
	public void verifyIfLibraryManagementDispaysOnLeftMenu()
	{
		videos =new Videos(driver);
		videos.verifyIfLibraryManagementDispaysOnLeftMenu();
		log.info("verifyIfLibraryManagementDispaysOnLeftMenu test completed");
	}
	
	@Test(priority=3,dependsOnMethods={"verifyIfLibraryManagementDispaysOnLeftMenu"})
    public void showEntetiesUnderLibraryManagement()
    {
    	videos.showEntetiesUnderLibraryManagement();
    }
	
	@Test(priority=4)
    public void verifyIfVideosDisplaysUnderLibraryManagement()
    {
    	videos.verifyIfVideosDisplaysUnderLibraryManagement();
    }
	
	@Test(priority=5)
	public void verifyNUmberOfVideosDisplayingOnFirstPage()
	{
		videos.verifyNumberOfVideosDisplayingOnFirstPage();
	}
	
	@Test(priority=6)
	public void verifyTotalNumberOfVideos()
	{
		videos.verifyNumberOfVideosDisplayingOnFirstPage();
	}
}
