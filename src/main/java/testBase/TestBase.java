package main.java.testBase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import main.java.waitHelper.WaitHelper;

public class TestBase {

	public WebDriver driver;
	WaitHelper waitHelper;
	public static Properties OR; 
	public Logger log = Logger.getLogger(TestBase.class);
	
	Calendar calendar = Calendar.getInstance();
	SimpleDateFormat formator = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");	
	ExtentReports extentReport = new ExtentReports(System.getProperty("user.dir") + "/src/main/java/report/test" + formator.format(calendar.getTime()) + ".html", true);
	public static ExtentTest extentTest;
	public ITestResult itestResult;
	
	public String loadPropertiesFile(String key) throws IOException
	{
		String log4j = "log4j.properties";
		PropertyConfigurator.configure(log4j);
		OR = new Properties();
		File f1 = new File(System.getProperty("user.dir")+"/src/main/java/config/config.properties");
		FileInputStream file = new FileInputStream(f1);
		OR.load(file);
		log.info("loading configuration properties");
		return OR.getProperty(key);
	}
	
	public String getScreenshot() throws IOException
	{
		TakesScreenshot screen = (TakesScreenshot)driver;
		File image = screen.getScreenshotAs(OutputType.FILE);
		String imageLocation = System.getProperty("user.dir")+"/src/main/java/screenshot/";
		String actualImageName = imageLocation +formator.format(calendar.getTime())+".png";
		File actucalImage = new File(actualImageName);
		FileUtils.copyFile(image, actucalImage);
		return actualImageName;
	}
	
	public void getBrowser(String browser)
	{
		if(browser.equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		
		else if(browser.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"/drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		}	
	}
	
	@BeforeTest
	public void launchBrowser() throws IOException
	{
		String browser = loadPropertiesFile("Browser");
		getBrowser(browser);
		log.info("launching browser");
		String url =loadPropertiesFile("Website");
		driver.get(url);
		log.info("hit url");
		driver.manage().window().maximize();
		waitHelper = new WaitHelper(driver);
		waitHelper.setImplicitWait(20, TimeUnit.SECONDS);
	}
	
	@BeforeMethod
	public void beforeMethod(Method method)
	{
		//log.info("before method initiated.");
		extentTest = extentReport.startTest(method.getName());
		extentTest.log(LogStatus.INFO, method.getName() + " test Started");
		waitHelper.setImplicitWait(30, TimeUnit.SECONDS);
		log.info("before method completed.");
	}
	
	@AfterMethod
	public void afterMethod(ITestResult result) throws IOException
	{
		//log.info("after method initiated.");
		if(result.getStatus() == ITestResult.SUCCESS)
		{
			extentTest.log(LogStatus.PASS, result.getName()+ " test is pass");
		}
		else if (result.getStatus() == ITestResult.FAILURE)
		{
			extentTest.log(LogStatus.FAIL, result.getName() + " test is fail" +result.getThrowable());
			String screenshot = getScreenshot();
			extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(screenshot));
		}
		else if(result.getStatus() == ITestResult.SKIP)
		{
			extentTest.log(LogStatus.SKIP, result.getName()+" test is skipped");
		}
		log.info("after method completed.");
	}
	
	@AfterClass
	public void afterClass()
	{
		//log.info("after class initiated");
		//driver.quit();
		extentReport.endTest(extentTest);
		extentReport.flush();
		log.info("after class completed");
	}
}
