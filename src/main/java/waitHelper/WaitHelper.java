package main.java.waitHelper;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitHelper {

	public WebDriver driver;
	
	Logger log = Logger.getLogger(WaitHelper.class);
	
	public WaitHelper(WebDriver driver)
	{
		this.driver = driver;
	}
	
	public void setImplicitWait(long timeout, TimeUnit unit) {
		log.info(timeout);
		driver.manage().timeouts().implicitlyWait(timeout, unit == null ? TimeUnit.SECONDS : unit);
		log.info("implicit wait is set for"+timeout+" "+unit);
	}
	
	public void setExplicitWait_ElementVisible( WebElement element, long timeout) {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.visibilityOf(element));
		log.info("element found..."+element.getText());
	}
	
	public void setExplicitWait_ElementInVisible(WebElement element,long timeout)
	{
		log.info("setExplicitWait_ElementInVisible initiated");
		WebDriverWait wait = new WebDriverWait(driver,timeout);
		wait.until(ExpectedConditions.invisibilityOf(element));
		log.info("toaster message is invisible...");
	}
	
	public void setExplicitWait_ElementClickable( long timeout,WebElement element)
	{
		log.info("setExplicitWait_ElementClickable initiated");
		WebDriverWait wait = new WebDriverWait(driver,timeout);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		log.info("element is clickable..."+element.getText());
	}
	
	public void setFluentWait(WebDriver driver, int timeOutInSeconds, int pollingEveryInMiliSec) {
		log.debug("");
		WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
		wait.pollingEvery(pollingEveryInMiliSec, TimeUnit.MILLISECONDS);
		wait.ignoring(NoSuchElementException.class);
		wait.ignoring(ElementNotVisibleException.class);
		wait.ignoring(StaleElementReferenceException.class);
		wait.ignoring(NoSuchFrameException.class);
	}
}
