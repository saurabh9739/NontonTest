package main.java.verificationHelper;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

public class VerificationHelper {

	static Logger log = Logger.getLogger(VerificationHelper.class);

	public static void verifyElementPresent( WebElement element) {
		if(element.isDisplayed())
		{
			log.info(element.getText()+" is dispalyed");
		}
		
		else
		{
			log.error("Element not found ");
		}	 
	}
	
	public static void verifyElementNotPresent( WebElement element) {
		if(element.isDisplayed())
		{
			log.error(element.getText()+" is dispalyed");
		}
		
		else
		{
			log.info("Element is not present ");
		}	 
	}
	
	public static void verifyTextEquals( String actualText,String expectedText) {
		if(actualText.equals(expectedText))
		{
			log.info("actualText is :"+actualText+" expected text is: "+expectedText);
			log.info("matched!");
		}
		else 
		{
			log.error("actualText is :"+actualText+" expected text is: "+expectedText);
			log.info("not matched!");
		}
		}
	}


