package main.java.pageObject;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;
import main.java.waitHelper.WaitHelper;

public class Videos {
	WebDriver driver;
	WaitHelper wait;
	Logger log = Logger.getLogger(Videos.class);
	SoftAssert softassert = new SoftAssert();
	
	public Videos(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
		wait = new WaitHelper(driver);
		wait.setExplicitWait_ElementVisible(createNewVideo,30);
	}
	
	@FindBy(how=How.XPATH,using="//*[contains(text(),'Library Management')]")
	WebElement libraryManagement;
	
	@FindBy(how=How.XPATH,using="//div[@id='m_aside_left']/div/ul[1]/li")
    List<WebElement> libraryManagementAndItsEnteties;
	
	@FindBy(how=How.XPATH,using="//*[contains(text(),'Create new Video')]")
	WebElement createNewVideo;
	
	@FindBy(how=How.XPATH,using="//div[@id='m_datatable']/table/tbody/tr")
	List<WebElement> listOfVideosOnFirstPage;
	
	@FindBy(how=How.XPATH,using="//div[@class='m-datatable__pager-info']/span")
	WebElement totalNUmberOfVideos;
	
	public void verifyIfLibraryManagementDispaysOnLeftMenu()
	{
		if(libraryManagement.isDisplayed())
		{
			softassert.assertTrue(libraryManagement.isDisplayed(), "Library management is diplayed on Left menu");
		}
		else
		{
			softassert.assertFalse(libraryManagement.isDisplayed(), "Library Management is not displayed on Left menu");
		}
		softassert.assertAll();
	}
	
	List<String> entetiesName = new ArrayList();
	public void showEntetiesUnderLibraryManagement()
	{
		log.info("The Entities under Library Management are as follows:");
		for(int i=1;i<libraryManagementAndItsEnteties.size();i++)
		{
			log.info(libraryManagementAndItsEnteties.get(i).getText());
			entetiesName.add(libraryManagementAndItsEnteties.get(i).getText());
		}
	}
	
	public void verifyIfVideosDisplaysUnderLibraryManagement()
	{
		int count =0;
		for(String entityName : entetiesName)
		{
			if(entityName.equals("Videos"))
			{
				count++;
			}
		}
		
		if(count==1)
		{
			softassert.assertTrue(true);
		}
		else
		{
			softassert.assertFalse(false);
		}
		softassert.assertAll();
	}
	
	int numberofVideos; 
	public void verifyNumberOfVideosDisplayingOnFirstPage()
	{
		numberofVideos = listOfVideosOnFirstPage.size();
		log.info("The number of videos listed in Fisrt page is: " +numberofVideos);
		if(numberofVideos>0)
		{
			softassert.assertNotEquals(0, numberofVideos > 0);
		}
		else
		{
			softassert.assertEquals(0, numberofVideos >0);
		}
		softassert.assertAll();
	}
	
	int totalNumderOfVideo;
	public void verifyTotalNumberOfVideos()
	{
		if(numberofVideos > 0)
		{
		String totalVideosInSentence = totalNUmberOfVideos.getText();
		String [] srtarray = totalVideosInSentence.split(" ");
		String video = srtarray[srtarray.length-1];
		totalNumderOfVideo = Integer.parseInt(video);
		log.info("the total available video is: "+totalNumderOfVideo);
		}
	}
}
