package main.java.pageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import main.java.waitHelper.WaitHelper;

public class LoginPage {

		WebDriver driver;
		Logger log = Logger.getLogger(LoginPage.class);
		WaitHelper waitHelper;
		
		public LoginPage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
			waitHelper = new WaitHelper(driver);
			waitHelper.setExplicitWait_ElementVisible(signin, 20);
		}
		
		
		@FindBy(how=How.XPATH,using="/html/body/div[3]/div/div/div/div[2]/form/div[1]/input")
		private WebElement email;
		
		@FindBy(how=How.ID, using="email-error")
		private WebElement emailError;
		
		@FindBy(how=How.NAME,using="password")
		private WebElement password;
		
		@FindBy(id="password-error")
		private WebElement passwordError;
		
		@FindBy(how=How.XPATH,using="/html/body/div[3]/div/div/div/div[2]/form/div[2]/div/span/span/i[1]")
		private WebElement showPassword;
		
		@FindBy(how=How.XPATH,using="/html/body/div[3]/div/div/div/div[2]/form/div[2]/div/span/span/i[2]")
		private WebElement hidePassword;
		
		@FindBy(how=How.ID,using="m_login_signin_submit")
		private WebElement signin;
		
		@FindBy(how=How.ID,using="m_login_forget_password")
		private WebElement forgotPassword;
		
		@FindBy(how=How.CLASS_NAME,using="toast-title ng-star-inserted")
		private WebElement toastMessage;
		
		SoftAssert softassert = new SoftAssert();
		
		public void enterEmail(String email)
		{
			log.info("entering email address...."+email);
			this.email.sendKeys(email);
		}
		
		public void clearEmail()
		{
			email.clear();
			log.info("email cleared...");
		}
		
		public void enterPassword(String password)
		{
			log.info("entering password...."+password);
			this.password.sendKeys(password);
		}
		
		public void clearPassword()
		{
			password.clear();
			log.info("password cleared...");
		}
		
		public void showPassword()
		{
			showPassword.click();
		}
		
		public void hidePasswod()
		{
			hidePassword.click();
		}
		
		public void clickSignIn()
		{
			log.info("clicked on sign in link...");
			signin.click();
		}
		
		public Videos clickSignInWithValidCredentials()
		{
			log.info("clicked on sign in link...");
			signin.click();
			if(signin.isEnabled())
			{
				softassert.assertTrue(signin.isSelected());
			}
			else
			{
				softassert.assertFalse(signin.isSelected());
			}
			
			softassert.assertAll();
			return new Videos(driver);
		}
		
		public ForgotPassword clickForgotPassword() 
		{
			log.info("clicked on forgot password link...");
			forgotPassword.click();
			return new ForgotPassword(driver);
		}
		
		public String getEmailErrorMesssage()
		{
			return emailError.getText();
		}
		
		public String getPasswordErrorMessage()
		{
			return passwordError.getText();
		}
		
		public String getToastMessage()
		{
			return toastMessage.getText();
		}
		
	}

	

