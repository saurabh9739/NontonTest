package main.java.pageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import main.java.waitHelper.WaitHelper;

public class ForgotPassword {

	WebDriver driver;
	Logger log = Logger.getLogger(LoginPage.class);
	WaitHelper waitHelper;
	
	@FindBy(xpath="//*[@id='m_email']")
	WebElement email;
	
	@FindBy(xpath="//*[@id='m_login_forget_password_submit']")
	WebElement request;
	
	@FindBy(xpath="//*[@id='m_login_forget_password_cancel']")
	WebElement cancel;
	
	public ForgotPassword(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		waitHelper = new WaitHelper(driver);
		waitHelper.setExplicitWait_ElementVisible(email, 30);
	}
	
	public void Email(String email)
	{
		log.info("entering email....."+ email);
		this.email.sendKeys(email);
	}
	
	public void Request()
	{
		log.info("clicking Request button...");
		request.click();
	}
	
	public LoginPage Cancel()
	{
		log.info("clicking Cancel Button...");
		cancel.click();
		return new LoginPage(driver);
	}
}
