package main.java.pageObject;
import org.testng.Assert;
import java.util.List;
import org.apache.log4j.Logger;
import org.junit.rules.Timeout;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;
import main.java.waitHelper.WaitHelper;

public class Genre {

	WebDriver driver;
	WaitHelper waithelper;
	Logger log = Logger.getLogger(Genre.class);
	
	public Genre(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
		waithelper = new WaitHelper(driver);
		waithelper.setExplicitWait_ElementVisible(firstGenreName, 60);
	}
	
	@FindBy(how=How.XPATH,using="//*[contains(text(),'Taxonomy Management')]")
	WebElement taxonomyManagement;
	
	@FindBy(how=How.LINK_TEXT,using="Genre")
	WebElement genre;
	
	@FindBy(how=How.XPATH,using="//div[@id='m_datatable']/table/tbody/div[1]/div/tr")
	List<WebElement> ListOfGenreOnFirstPage;
	
	@FindBy(how=How.ID,using="//div[@id='m_datatable']/div[1]/div/div/button/span[1]")
	WebElement defaultPagination;
	
	@FindBy(how=How.XPATH,using="//div[@id='m_datatable']/div/div/span")
	WebElement displayingXrecordsOfYrecords;
	
	@FindBy(how=How.ID,using="m_datatable_publish_all")
	WebElement publishButton;
	
	@FindBy(how=How.ID,using="m_datatable_unpublish_all")
	WebElement unpublishButton;
	
	@FindBy(how=How.CLASS_NAME,using="toast-title ng-star-inserted")
	WebElement toastMessage;
	
	@FindBy(how=How.CLASS_NAME,using="toast-message ng-star-inserted")
	WebElement toastMessageToSelectAtleastOneGenre;
	
	@FindBy(how=How.XPATH,using="//div[@id='m_datatable']/table/thead/tr/th[1]/span")
	WebElement selectAllGenreCheckBox;
	 
	@FindBy(how=How.XPATH,using="//div[@id='m_datatable']/table/tbody/div[1]/div/tr[1]/td[1]/span/label/span")
    WebElement selectSingleGenreCheckBox;
    
	@FindBy(how=How.ID,using="filterDropdown")
    WebElement filters;
    
	@FindBy(how=How.XPATH,using="//div[@class='m-checkbox-list']/label[1]/span")
    WebElement publishedInFilter;
    
	@FindBy(how=How.XPATH,using="//div[@class='m-checkbox-list']/label[2]/span")
    WebElement unpublishedInFilter;
    
	@FindBy(how=How.XPATH,using="//div[@class='m-checkbox-list']/label[2]/span")
    WebElement selectGenreValidtionMessage;
	
	@FindBy(how=How.XPATH,using="//div[@class='m-alert m-alert--icon m-alert--outline alert alert-primary']")
    WebElement alertPopUpForPublishUnpublishGenre;
	
	@FindBy(how=How.XPATH,using="//div[@class='m-alert m-alert--icon m-alert--outline alert alert-primary']/div[3]/button[1]")
    WebElement alertPopUpYesPublishUnpublishGenreButton;
	
	@FindBy(how=How.XPATH,using="//div[@class='m-alert m-alert--icon m-alert--outline alert alert-primary']/div[3]/button[2]")
    WebElement alertPopUpCancelPublishUnpublishGenreButton;
	
	@FindBy(how=How.XPATH,using="//button[text()='Apply Filter']")
    WebElement applyFilter;
    
	@FindBy(how=How.XPATH,using="//div[@id='m_datatable']/table/tbody/div/div/tr[1]/td[3]")
    WebElement firstGenreName;
    
	@FindBy(how=How.XPATH,using="//div[@id='m_datatable']/table/tbody/div[1]/div/tr[1]/td[5]/span/a[1]/i")
    WebElement publishUnpublishUnderAction;
	
	SoftAssert softassert = new SoftAssert();
	
	
	public void verifyIfTaxonomyManagementDisplaysOnPage()
	{
		if(taxonomyManagement.isDisplayed())
		{
			log.info("Taxonomy Management is displaying on page");
			softassert.assertTrue(taxonomyManagement.isDisplayed());
		}
		else
		{
			log.error("Taxonomy Management is not displaying on page");
			softassert.assertFalse(taxonomyManagement.isDisplayed());
		}
		softassert.assertAll();
	}
	
	public void verifyIfGenreListedUnderTaxonomyManagement()
	{
		if(taxonomyManagement.isDisplayed())
		{
        List <WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'Taxonomy Management')]//following::li"));
		
		int count=0;
		String [] arr = new String[list.size()];
		for (WebElement webElement : list) {
            //System.out.println(webElement.getText());  
           log.info("The contents below Taxonomy Management are: "+webElement.getText());
            arr[count]= webElement.getText();
            count++; 
        }
		
		int i;
		for(i=0;i<arr.length;i++)
		{
			if(arr[i].contains("Genre"))
			{
				for(int j=i+1;j<=arr.length;j++)
				{
					if(arr[j].contains("USER MANAGEMENT"))
					{
						log.info("Genre is placed under TaxonomyManagement");
						softassert.assertTrue(genre.isDisplayed());
						break;
					}
					else if(j==arr.length)
					{
						log.error("Genre is not placed under TaxonomyManagement");
						softassert.assertFalse(genre.isDisplayed());
					}
				}
			}
			
		 }
	   }
		softassert.assertAll();
	}
	
	public void countNumberOfGenreDisplayingOnFirstPage()
	{
		if(ListOfGenreOnFirstPage.size()>0)
		{
			softassert.assertFalse(ListOfGenreOnFirstPage.isEmpty());
			log.info("the total number of Genre displaying on 1st page is: "+ListOfGenreOnFirstPage.size());
	    }
		else
		{
			softassert.assertTrue(ListOfGenreOnFirstPage.isEmpty());
			log.error("no genre is displaying on page");
		}
		softassert.assertAll();
	}
	
	public void clickPublishSelectedWithoutselectingGenre()
	{
		publishButton.click();
		log.info("Publish Button is clicked");
	    String expectedErrorMessage = "Please select atleast one genre";
		String actualErrorMessage = toastMessageToSelectAtleastOneGenre.getText();
		Assert.assertEquals(actualErrorMessage, expectedErrorMessage);
		waithelper.setExplicitWait_ElementInVisible(toastMessageToSelectAtleastOneGenre,90 );
	}
	
	public void clickUnpublishSelectedWithoutselectingGenre()
	{
		unpublishButton.click();
		log.info("Unpublished button is clicked");
		String expectedErrorMessage = "Please select atleast one genre";
		String actualErrorMessage = toastMessageToSelectAtleastOneGenre.getText();
		Assert.assertEquals(actualErrorMessage, expectedErrorMessage);
		waithelper.setExplicitWait_ElementInVisible(toastMessageToSelectAtleastOneGenre,90 );
	}
	
	public void selectAllGenre()
	{
		selectAllGenreCheckBox.click();
		log.info("selected all Genres");
		if(selectAllGenreCheckBox.isSelected())
		{
			softassert.assertTrue(selectAllGenreCheckBox.isSelected());
		}
		else
		{
			softassert.assertFalse(selectAllGenreCheckBox.isSelected());
		}
		softassert.assertAll();
	}
	
	public void selectFirstGenre()
	{
		selectSingleGenreCheckBox.click();
		log.info("selected first Genres");
		if(selectSingleGenreCheckBox.isSelected())
		{
			softassert.assertTrue(selectSingleGenreCheckBox.isSelected());
		}
		else
		{
			softassert.assertFalse(selectSingleGenreCheckBox.isSelected());
		}
		softassert.assertAll();
	}
	
	public void clickYesPublishButtonOnAlertPopUp()
	{
		alertPopUpYesPublishUnpublishGenreButton.click();
		log.info("alertPopUpYesPublishUnpublishGenreButton is clicked");
		if(alertPopUpYesPublishUnpublishGenreButton.isSelected())
		{
			softassert.assertTrue(alertPopUpYesPublishUnpublishGenreButton.isSelected());
		}
		else
		{
			softassert.assertFalse(alertPopUpYesPublishUnpublishGenreButton.isSelected());
		}
		softassert.assertAll();
	}
	
	public void clickCancelButtonOnAlertPopUp()
	{
		alertPopUpCancelPublishUnpublishGenreButton.click();
		log.info("alertPopUpCancelPublishUnpublishGenreButton is clicked");
		if(alertPopUpCancelPublishUnpublishGenreButton.isSelected())
		{
			softassert.assertTrue(alertPopUpCancelPublishUnpublishGenreButton.isSelected());
		}
		else
		{
			softassert.assertFalse(alertPopUpCancelPublishUnpublishGenreButton.isSelected());
		}	
		softassert.assertAll();
	}
	
	public void clickFilters()
	{
		filters.click();
		log.info("Filters has been clicked");
		if(filters.isSelected())
		{
			softassert.assertTrue(filters.isSelected());
		}
		else
		{
			softassert.assertFalse(filters.isSelected());
		}
		softassert.assertAll();
	}
	
	public void clickPublishedInFilters()
	{
		publishedInFilter.click();
		log.info("Published checkbox in filter has been clicked");
		if(publishedInFilter.isSelected())
		{
			softassert.assertTrue(publishedInFilter.isSelected());
		}
		else
		{
			softassert.assertFalse(publishedInFilter.isSelected());
		}
		softassert.assertAll();
	}
	
	public void clickUnpublishedInFilters()
	{
		unpublishedInFilter.click();
		log.info("Unpublished checkbox in filter has been clicked");
		if(unpublishedInFilter.isSelected())
		{
			softassert.assertTrue(unpublishedInFilter.isSelected());
		}
		else
		{
			softassert.assertFalse(unpublishedInFilter.isSelected());
		}
		softassert.assertAll();
	}
	
	public void clickAppyFilter()
	{
		applyFilter.click();
		log.info("Apply Filter button has been clicked");
		if(applyFilter.isSelected())
		{
			softassert.assertTrue(applyFilter.isSelected());
		}
		else
		{
			softassert.assertFalse(applyFilter.isSelected());
		}
		softassert.assertAll();
	}
	
	public void verifyIfDefaultPaginationIsMarkedAsTen()
	{
		defaultPagination.getText();
		log.info("default pagination is: "+defaultPagination.getText());
	}
	
	public void countTotalNUmberOfGenre()
	{
		String displayingRecords = displayingXrecordsOfYrecords.getText();
	    int IndexOfOF = displayingRecords.indexOf("of");
	    int IndexOfRecords = displayingRecords.indexOf("records");
	    log.info("displaying index of first word "+displayingRecords.indexOf("of"));
	    log.info("displaying index of last word "+displayingRecords.indexOf("records"));
	    String totalGenre = displayingRecords.substring(IndexOfOF, IndexOfRecords);
	    log.info("total Genre :" +totalGenre);
	}
}
