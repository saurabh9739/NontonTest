package main.java.pageObject;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.eclipse.jetty.util.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import main.java.waitHelper.WaitHelper;

public class CMSusers {
	
	WebDriver driver;
	WaitHelper waithelper;
	Logger log = Logger.getLogger(LoginPage.class);
	
	@FindBy(how=How.XPATH,using="//*[@id='m_aside_left']/div/ul/li")
	List<WebElement> leftMenuEntities;
	
	@FindBy(how=How.XPATH,using="//*[@class='m-portlet__body']/div[1]/table/tbody/div/div/tr")
	List<WebElement> listofCMSusers;
	
	@FindBy(how=How.XPATH,using="//div[@id='m_datatable']/table/tbody/div/div/tr[1]/td[1]")
	WebElement FirstUser;
	
	@FindBy(how=How.LINK_TEXT,using="Add new User")
	WebElement AddNewUser;
	
	@FindBy(how=How.LINK_TEXT,using="Genre")
	WebElement genre;
	
	SoftAssert softassert = new SoftAssert();
	
	public CMSusers(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
		waithelper = new WaitHelper(driver);
		waithelper.setExplicitWait_ElementVisible(FirstUser, 30);
	}
	
	public void leftMenuEntity()
	{
		List<String> actualList= new ArrayList();
		log.info("");
		log.info("The entities are as follows: ");
		for(WebElement entity : leftMenuEntities)
		{
			log.info(entity.getText());
			actualList.add(entity.getText());
		}
		log.info("");
		
        List<String> expectedList= new ArrayList();
        expectedList.add("LIBRARY MANAGEMENT");
        expectedList.add("Videos [WIP]");
        expectedList.add("Series");
        expectedList.add("TAXONOMY MANAGEMENT");
        expectedList.add("Genre");
        expectedList.add("Cast and Crew");
        expectedList.add("Content Provider");
        expectedList.add("USER MANAGEMENT");
        expectedList.add("CMS Users [WIP]");
        
        softassert.assertEquals(actualList.size(), expectedList.size());
        
        for(int i=0;i<actualList.size();i++)
    	{
        	softassert.assertEquals(actualList.get(i), expectedList.get(i));
    	}
        softassert.assertAll();
	}
	
	public CreateNewUser clickAddNewUser()
	{
		AddNewUser.click();
		log.info("Add New User button is clicked");
		return new CreateNewUser(driver);
	}
	
	public void countCMSUsers()
	{
		//List<WebElement> listofCMSusers= driver.findElements(By.xpath("//*[@class='m-portlet__body']/div[1]/table/tbody/div/div/tr"));	
		log.info("total number of CMSusers are: "+listofCMSusers.size());
	}
	
	public Genre clickGenreTab() throws InterruptedException
	{
		genre.click();
		log.info("Genre Tab is clicked");
		if(genre.isSelected())
		{
			softassert.assertTrue(genre.isSelected());
		}
		else
		{
			softassert.assertFalse(genre.isSelected());
		}
		softassert.assertAll();
		Thread.sleep(3000);
		return new Genre(driver);
	}
}
